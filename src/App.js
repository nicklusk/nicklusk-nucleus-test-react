import React, { Component } from 'react';
import axios from 'axios';

import logo from './logo.svg';
import './App.css';

const URL = 'https://nucleus-test-service.herokuapp.com/api/messages';

export default class App extends Component {
 constructor(props) {
   super(props);
   this.state = {
     items: []
   }
 }

 componentDidMount() {
   var _this = this;
   axios.get(URL)
   .then(function(res){
     console.log(res)
     _this.setState({
       items: res.data.Payload
     });
   })
   .catch(function(e) {
     console.log('ERROR ', e);
   })
 }

 render() {
   const renderItems = this.state.items.map(function(item, i) {
     return (
       <li key={i}>{item.id}<br />{item.title}<br />{item.body}<br />{item.sender}<br />{item.recipient}<br />{item.sentDate}</li>
     );
   });
   console.log(renderItems)
   return (
     <div className="App">
       <div className="App-header">
         <img src={logo} className="App-logo" alt="logo" />
         <h2>Welcome to Nucleus React Test</h2>
       </div>
     <ul>
       {renderItems}
     </ul>
     </div>
   );
 }
}
